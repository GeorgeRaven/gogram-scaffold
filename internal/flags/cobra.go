package flags

import (
	"fmt"
	"reflect"

	"github.com/spf13/cobra"
)

// BindStructToCobraCommand binds a structs firelds to a cobra command
// This uses struct tags to determine the names of the flags, descritpions and default values
func BindStructToCobraCommand(cmd *cobra.Command, config interface{}, prefix string) error {
	fmt.Printf("config: %v\n", config)
	t := reflect.TypeOf(config)
	v := reflect.ValueOf(config)
	fmt.Printf("v: %v\n", v)

	for i := 0; i < t.NumField(); i++ {
		ft := t.Field(i)
		fv := v.Field(i)
		fn := ft.Name
		fmt.Println(fn)
		fmt.Println(fv)
		fmt.Println(ft.Type.Kind())

		switch ft.Type.Kind() {
		case reflect.Int:
			cmd.Flags().IntP(fn, "", int(fv.Int()), "")
		case reflect.String:
			cmd.Flags().StringP(fn, "", fv.String(), "")
		case reflect.Bool:
			cmd.Flags().BoolP(fn, "", fv.Bool(), "")
		default:
			// Return an error if the type is not supported
			return fmt.Errorf("'%v' is an unsupported type: '%v'\n", fn, ft.Type.Kind())
		}
	}
	return nil
}
