# Gogram-Scaffold

## Safe Structured Logging

This scaffold makes use of slog for structured logging, for log levels like info, debug, error.
It is also good practice to use slog.Any, slog.Int, etc, in attribute pairs to slog.
This is because you will see no error if you dont do this and will silently fail.

```go

loggerHandler := slog.NewTextHandler(os.Stdout, nil)
logger := slog.New(loggerHandler)
logger.Info("Hellow World", slog.Any("key", "value")) // Safe will error at compile
logger.Info("Hellow World", "key", "value")           // unsafe will silently "!BADKEY"

```

## Dependency Injection with Closures

This scaffold makes use of the closure pattern to provide dependency injection across modules, since basic dependency injection cannot work between modules. This closure-dependency-injection looks like:

```go

// config/config.go

type Application struct {
	Logger *slog.Logger
}

// routes/routes.go

func ClosureHello(app *config.Application) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World!"))
	}
}

```

## Centralised Error Handling

This scaffold adds centralised error handling to make it easier and more consistent to return errors to the user, and to log said errors using the aforementioned structured logger.

## Concurrency Safe PostgreSQL Database Driver with Connection Pooling
