/*
Copyright © 2024 George Onoufriou <GeorgeRavenCommunity+Gogram@pm.me>
*/
package main

import "gitlab.com/georgeraven/gogram-scaffold/cmd"

func main() {
	cmd.Execute()
}
