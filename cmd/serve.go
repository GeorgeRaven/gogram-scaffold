/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/georgeraven/gogram-scaffold/cmd/api"
	apiConf "gitlab.com/georgeraven/gogram-scaffold/cmd/api/config"
	vflags "gitlab.com/georgeraven/gogram-scaffold/internal/flags"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Serve an API server",
	Long: `Serve and example API server backed with a PostgreSQL database:

Please ensure the database is running before before using this command.`,
	Run: func(cmd *cobra.Command, args []string) {
		api.Serve(cmd)
	},
}

func init() {
	rootCmd.AddCommand(serveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	conf := apiConf.Config{}
	err := vflags.BindStructToCobraCommand(serveCmd, conf, "")
	if err != nil {
		panic(err)
	}
}
