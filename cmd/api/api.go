package api

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"os"

	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/config"
	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/helpers"
	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/routes"

	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

func cobraToStruct(cmd *cobra.Command) *config.Config {
	config := &config.Config{}
	cmd.Flags().VisitAll(func(f *pflag.Flag) {
		// Determine the naming convention of the flags when represented in the config file
		configName := f.Name
		fmt.Println(configName)
	})
	return config
}

func Serve(cmd *cobra.Command) {
	cfg := cobraToStruct(cmd)
	// Logger options to log level debug
	logOpts := &slog.HandlerOptions{
		//AddSource: true,
		Level: slog.LevelDebug,
	}
	// Register handler in two steps to enable easy overriding
	// from CLI options eg a json log handler:
	// loggerHandler := slog.NewJSONHandler(os.Stdout, logOpts)
	loggerHandler := slog.NewTextHandler(os.Stdout, logOpts)
	logger := slog.New(loggerHandler)

	// Register concurrency safe postgresql db pool
	psqlUrl, err := helpers.ConstructPostgresUrl(cfg.Postgres)
	if err != nil {
		logger.Error("Failed to construct postgres url", slog.Any("error", err))
		os.Exit(1)
	}
	dbpool, err := pgxpool.New(context.Background(), psqlUrl)
	if err != nil {
		logger.Error("Failed to connect to database", slog.Any("error", err))
		os.Exit(1)
	}
	defer dbpool.Close()
	var greeting string
	err = dbpool.QueryRow(context.Background(), "select 'Hello, world!'").Scan(&greeting)
	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}
	// Dependency injection struct to be passed into closures
	app := &config.Application{
		Logger: logger,
		Config: cfg,
	}
	mux := routes.Routes(app)
	// Use safe logger key value pairs using slog.Any, slog.Time, slog.Int etc
	logger.Info("Starting server", slog.Any("config", *app))

	err = http.ListenAndServe(app.Config.Host+":"+app.Config.Port, mux)
	logger.Error("Failed to start server", slog.Any("error", err))
}
