package helpers

import (
	"errors"
	"log/slog"
	"net/http"
	"runtime/debug"

	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/config"
)

func ServerError(app *config.Application) func(http.ResponseWriter, *http.Request, error) {
	return func(w http.ResponseWriter, r *http.Request, err error) {
		var (
			method = r.Method
			uri    = r.URL.RequestURI()
			trace  = string(debug.Stack())
		)
		app.Logger.Error(err.Error(), slog.String("method", method), slog.String("uri", uri), slog.String("trace", trace))
	}
}

func ClientError(app *config.Application) func(http.ResponseWriter, int) {
	return func(w http.ResponseWriter, status int) {
		http.Error(w, http.StatusText(status), status)

	}
}

// ConstructPostgresUrl constructs the postgres url from config struct
func ConstructPostgresUrl(pconf *config.PostgresConfig) (string, error) {
	if pconf == nil {
		return "", errors.New("postgres config is nil")
	}
	return "postgresql://" + pconf.User + ":" + pconf.Password + "@" + pconf.Host + ":" + pconf.Port + "/" + pconf.Database, nil
}
