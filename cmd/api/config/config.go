// Package config defines the configuration of the api server, along with the dependency injection
// in preperation for various cross-package routes. This uses the closure pattern.
package config

import "log/slog"

type Config struct {
	Host string `json:"host"`
	Port string `json:"port"`

	Postgres *PostgresConfig `json:"postgres"`
}

type PostgresConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Database string `json:"database"`
}

type Application struct {
	Logger *slog.Logger
	Config *Config
}
