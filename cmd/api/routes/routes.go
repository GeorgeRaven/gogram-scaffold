// Package routes defines the routes of the api server using closures for dependency injection
// This allows us to use external dependencies to the module and expand them in the future
package routes

import (
	"errors"
	"net/http"

	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/config"
	"gitlab.com/georgeraven/gogram-scaffold/cmd/api/helpers"
)

func Routes(app *config.Application) *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/hello", ClosureHello(app))
	mux.HandleFunc("/server-error", ServerError(app))
	return mux
}

// ClosureHello shows how to use closures for dependency injection
// Usually we would use a route like this if we had local dependencies
//
//	func (app *config.Application) Hello(w http.ResponseWriter, r *http.Request) {
//		w.Write([]byte("Hello World!"))
//	}
//
// But we can also use closures to do this instead by nesting the functions,
// we can still use outer variables from the outer function even after it has closed.
func ClosureHello(app *config.Application) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World!"))
	}
}

func ServerError(app *config.Application) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		helpers.ServerError(app)(w, r, errors.New("this is an error"))
	}
}
